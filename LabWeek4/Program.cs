﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

class Program
{
    static void Main()
    {
        Console.WriteLine("Computação em lote sequencial:");
        ProcessarLoteSequencial();

        Console.WriteLine("\nComputação em lote com threads:");
        ProcessarLoteComThreads();

        Console.ReadLine();
    }

    static void ProcessarLoteSequencial()
    {
        List<int> dados = ObterDados();

        Stopwatch stopwatch = Stopwatch.StartNew();

        foreach (var item in dados)
        {
            ProcessarItem(item);
        }

        stopwatch.Stop();
        Console.WriteLine($"Tempo total sequencial: {stopwatch.ElapsedMilliseconds} ms");
    }

    static void ProcessarLoteComThreads()
    {
        List<int> dados = ObterDados();
        int threadsCount = 3; // Número de threads a serem utilizadas
        SemaphoreSlim semaphore = new SemaphoreSlim(threadsCount); // Limite de threads

        Stopwatch stopwatch = Stopwatch.StartNew();

        // Dividir os dados em batches para distribuir entre as threads
        List<List<int>> batches = DividirEmBatches(dados, threadsCount);

        // Usar Task.Factory.StartNew para criar e iniciar threads
        List<Task> tasks = new List<Task>();

        foreach (var batch in batches)
        {
            tasks.Add(Task.Factory.StartNew(() =>
            {
                semaphore.Wait(); // Aguardar permissão do semáforo
                try
                {
                    foreach (var item in batch)
                    {
                        ProcessarItem(item);
                    }
                }
                finally
                {
                    semaphore.Release(); // Liberar a permissão do semáforo
                }
            }));
        }

        // Aguardar a conclusão de todas as tarefas
        Task.WaitAll(tasks.ToArray());

        stopwatch.Stop();
        Console.WriteLine($"Tempo total com threads: {stopwatch.ElapsedMilliseconds} ms");
    }

    static List<int> ObterDados()
    {
        // Simula a obtenção de dados de alguma fonte
        return new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    }

    static void ProcessarItem(int item)
    {
        // Simula o processamento de um item
        Console.WriteLine($"Processando item: {item} - Thread: {Thread.CurrentThread.ManagedThreadId}");
        Thread.Sleep(1000); // Simula uma operação demorada
    }

    static List<List<int>> DividirEmBatches(List<int> dados, int batchSize)
    {
        // Divide a lista de dados em batches
        List<List<int>> batches = new List<List<int>>();

        for (int i = 0; i < dados.Count; i += batchSize)
        {
            batches.Add(dados.GetRange(i, Math.Min(batchSize, dados.Count - i)));
        }

        return batches;
    }
}
